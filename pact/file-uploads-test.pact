;(namespace "free")
;define keyset here to guard smart contract
; uncomment next line and enter your keyset name
;   note that you must provide this keyset named "ks" in the transaction env data
;(define-keyset "YOUR-KEYSET" (read-keyset "ks"))
(define-keyset 'upload-admin-keyset
  (read-keyset "upload-admin-keyset"))

(module file-uploads-test GOVERNANCE
  "A smart contract to greet the world."

  (defcap GOVERNANCE ()
    "defines who can update the smart contract"
    true
    ; currently anyone can change your smart contract
    ; for added security replace 'true' with a keyset or account or make it immutable
    ;   keyset:
    ;     (enforce-guard (keyset-ref-guard 'YOUR-KEYSET))
    ;   account:
    ;     (enforce-guard (at 'guard (coin.details "YOUR-ACCOUNT-NAME")))
    ;   immutable:
    ;     false
  )

  (defschema upload
    @doc "Schema to store uri and blockheight"
    @model [(invariant (!= uri ""))]
    uri:string
    block-height:integer)

  (deftable uploaded-table:{upload})

  (defun new-upload (uploadUri:string)
    "Designed for /send calls. Leave your trace on Kadena mainnet!"
    (enforce (!= uploadUri "") "Name cannot be empty")
    (insert uploaded-table uploadUri
      { "uri"         : uploadUri,
        "block-height" : (at "block-height" (chain-data)) }
    )
    (format "{} your uri." [uploadUri])
  )

  (defun lookup (key:string)
    (read uploaded-table key)
  )

  (defun get-all ()
    (map (read uploaded-table) (keys uploaded-table))
)

)
(create-table uploaded-table)
