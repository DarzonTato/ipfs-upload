import React, { useState, useRef, useEffect, Fragment } from "react";
import Pact from "pact-lang-api";

import Card from "../UI/Card";
import Button from "../UI/Button";
import ErrorModal from "../UI/ErrorModal";
import classes from "./Upload.module.css";

import { create } from "ipfs-http-client";

// import {v4 as uuidv4} from 'uuid';

import { kadenaAPI } from "../../kadena-config.js";

//Declare IPFS
const ipfsClient = create("https://ipfs.infura.io:5001/api/v0");

// const KP = Pact.crypto.genKeyPair();
// const API_HOST = "http://localhost:9001";

const Upload = (props) => {
  const nameInputRef = useRef();
  const descriptionInputRef = useRef();
  const imageInputRef = useRef();
  const attributesInputRef = useRef();

  const [error, setError] = useState();

  const [imageBuffer, setimageBuffer] = useState({
    buffer: "",
    type: "",
    name: "",
  });

  const [dataStatus, setDataStatus] = useState("");

  //useState api
  //  keep track of the blockchain call status
  const [txStatus, setTxStatus] = useState("");
  //  blockchain transaction result
  const [tx, setTx] = useState({});

  const connectWallet = async () => {
    const host = "http://localhost:9467/v1/accounts";
    const res = await fetch(host, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        asset: "kadena",
      }),
    });

    return await res.json();
  };

  const checkWallet = async () => {
    // check if already exist

    if (localStorage.getItem("accountAddress")) {
      return {
        status: "success",
        data: JSON.parse(localStorage.getItem("accountAddress")),
      };
    }

    // else connect and store
    try {
      alert("Please connect your wallet!");
      const result = await connectWallet();

      if (result.status === "success") {
        localStorage.setItem("accountAddress", JSON.stringify(result.data));
      }

      return result;
    } catch (error) {
      console.log("im here utility catch");
      return { status: "error", data: error };
    }
  };

  // Get file from user
  const CaptureFile = (event) => {
    event.preventDefault();
    //get the file from the input type file
    const file = event.target.files[0];
    const reader = new window.FileReader();
    // convert it to buffer
    reader.readAsArrayBuffer(file);
    reader.onloadend = () => {
      setimageBuffer({
        buffer: Buffer(reader.result),
        type: file.type,
        name: file.name,
      });
    };
  };
  //console.log('buffer', imageBuffer)

  const { status, data } = checkWallet();

  const uploadHandler = async (event) => {
    setDataStatus("signing");
    try {
      const ipfsBaseUrl = "https://ipfs.infura.io/ipfs/";
      event.preventDefault();
      const enteredName = nameInputRef.current.value;
      const enteredUDescription = descriptionInputRef.current.value;
      //const enteredImage = imageInputRef.current.value;
      const enteredAttributes = attributesInputRef.current.value;
      if (
        enteredName.trim().length === 0 ||
        enteredUDescription.trim().length === 0 ||
        enteredAttributes.trim().length === 0
      ) {
        setError({
          title: "Invalid input",
          message: "Please enter a valid name and age (non-empty values).",
        });
        return;
      }
      // upload image to IPFS
      const addedImage = await ipfsClient.add(imageBuffer.buffer);
      const metaDataObj = {
        name: enteredName,
        image: ipfsBaseUrl + addedImage.path,
        description: enteredUDescription,
        attributes: enteredAttributes,
      };
      // upload metadataobj to IPFS
      const addedMetaData = await ipfsClient.add(JSON.stringify(metaDataObj));
      //console.log(ipfsBaseUrl + addedMetaData.path);
      const uploadUri = ipfsBaseUrl + addedMetaData.path;

      // save to emulated blockchain
      // const cmds = [
      //   {
      //      keyPairs: KP,
      //      pactCode: Pact.lang.mkExp('file-uploads-test.new-upload', uploadUri),
      //   }
      //  ]
      // Pact.fetch.send(cmds, API_HOST)

      //save to testnet
      const caps = Pact.lang.mkCap(
        "Some Role here",
        "Some description",
        "free.fil-uploads-test-gas-station.GAS_PAYER",
        ["hi", { int: 2 }, 1.0]
      );

      var account = JSON.parse(localStorage.getItem("accountAddress"))[0];
      var pubkey =
        JSON.parse(localStorage.getItem("accountAddress"))[0].at(0) === "k"
          ? JSON.parse(localStorage.getItem("accountAddress"))[0].slice(2)
          : JSON.parse(localStorage.getItem("accountAddress"))[0];
      try {
        //creates transaction to send to wallet
        const toSign = {
          pactCode: `(${kadenaAPI.contractAddress}.new-upload ${JSON.stringify(
            uploadUri
          )})`,
          caps: caps,
          envData: {},
          sender: account, // change this to the account name of your zelcore
          chainId: kadenaAPI.meta.chainId,
          gasLimit: kadenaAPI.meta.gasLimit,
          gasPrice: kadenaAPI.meta.gasPrice,
          signingPubKey: pubkey, // account with no prefix k here
          networkId: kadenaAPI.meta.networkId,
          nonce: kadenaAPI.meta.nonce,
        };
        //sends transaction to wallet to sign and awaits signed transaction
        const signed = await Pact.wallet.sign(toSign);
        console.log(signed);
        //sends signed transaction to blockchain
        const tx = await Pact.wallet.sendSigned(signed, kadenaAPI.meta.host);
        //set html to wait for transaction response
        //set state to wait for transaction response
        setDataStatus("sending");
        setTxStatus("pending");
        try {
          //listens to response to transaction sent
          //  note method will timeout in two minutes
          //    for lower level implementations checkout out Pact.fetch.poll() in pact-lang-api
          let res = await Pact.fetch.listen(
            { listen: tx.requestKeys[0] },
            kadenaAPI.meta.host
          );
          console.log(res);
          //keep transaction response in local state
          setTx(res);
          if (res.result.status === "success") {
            //set state for transaction success
            alert("Success!");
            setTxStatus("success");
            setDataStatus("");
          } else {
            //set state for transaction failure
            alert("Failed!");
            setTxStatus("failure");
            setDataStatus("");
          }
        } catch (e) {
          console.log(e);
          //set state for transaction listening timeout
          alert("Timeout!");
          setTxStatus("timeout");
          setDataStatus("");
        }
      } catch (e) {
        console.log(e);
        //set state for transaction construction error
        alert("Validation Error!");
        setTxStatus("validation-error");
        setDataStatus("");
      }
    } catch (err) {
      console.log(err);
    }

    nameInputRef.current.value = "";
    descriptionInputRef.current.value = "";
    imageInputRef.current.value = "";
    attributesInputRef.current.value = "";
    setDataStatus("");
  };

  const errorHandler = () => {
    setError(null);
  };

  return (
    <Fragment>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
      <Card className={classes.input}>
        {dataStatus === "signing" && (
          <h1>Please sign transaction in your wallet...</h1>
        )}
        {dataStatus === "sending" && <h1>Sending data to blockchain...</h1>}
        {dataStatus === "" && (
          <form onSubmit={uploadHandler}>
            <label htmlFor="image">Image</label>
            <input
              id="image"
              type="file"
              ref={imageInputRef}
              onChange={CaptureFile}
            />
            <label htmlFor="name">Name</label>
            <input id="name" type="text" ref={nameInputRef} />
            <label htmlFor="description">Description</label>
            <input id="description" type="text" ref={descriptionInputRef} />
            <label htmlFor="attributes">Attributes</label>
            <input id="text" type="attributes" ref={attributesInputRef} />
            <Button type="submit">Upload</Button>
          </form>
        )}
      </Card>
    </Fragment>
  );
};

export default Upload;
